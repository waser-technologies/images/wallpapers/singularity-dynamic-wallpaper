#!/bin/bash

THEME=singularity-spacetime

if (( $EUID == 0 )); then
	if [ ! -d "/usr/share/backgrounds/$THEME" ]; then
		mkdir -p /usr/share/backgrounds/$THEME
		mkdir -p /usr/share/gnome-background-properties
	fi
	INSTALL_DIR="/usr/share/backgrounds/$THEME"
	PROPER_DIR='/usr/share/gnome-background-properties'
else
	echo
	echo "You need to run as superuser"
	echo
	exit 0 
fi

echo "Installing dynamic wallpapers images in $INSTALL_DIR"
cp -f *.png *.xml $INSTALL_DIR 

echo "Installing dynamic wallpapers property in $INSTALL_DIR"
cp -f $THEME-property.xml $PROPER_DIR

echo
echo "Done."
echo
echo "To remove run:
sudo rm -R $INSTALL_DIR && sudo rm $PROPER_DIR/$THEME-property.xml"
echo
exit 0

